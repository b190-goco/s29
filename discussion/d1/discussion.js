// SECTION
// Comparisons Query Operators

// $gt/$gte Operator
	/*
		- allows us to find documents that have field number values greater than or greater than or equal to a specified number

		SYNTAX:
			db.collectionName({ field: { $gt: value} });
			db.collectionName({ field: { $gte: value} });
	*/

	db.users.find({ age: { $gt: 50 }});
	db.users.find({ age: { $gte: 50 }});

// $lt/$lte Operator
	/*
		- allows us to find documents that have field number values less than or less than or equal to a specified number
		
		SYNTAX:
			db.collectionName({ field: { $lt: value} });
			db.collectionName({ field: { $lte: value} });
	*/

	db.users.find({ age: { $lt: 50 }});
	db.users.find({ age: { $lte: 50 }});

// $ne Operator
	/*
		- allows us to find documents that have field number not equal to a specified number
		
		SYNTAX:
			db.collectionName({ field: { $nen: value} });
	*/

	db.users.find({ age: { $ne: 82 }});

// $in Operator
	/*
		- allows us to find documents that have field with specific match criteria using different values
		
		SYNTAX:
			db.collectionName({ field: { $in: value} });
	*/

	db.users.find({ lastName: { $in: ["Armstrong","Doe"] }});

// SECTION
// $or Operator
/*
	- allows us to find documents matching a single criteria from multiple provided search criteria

	SYNTAX:
		db.collectionName({ $or: [ {fieldA: valueA}, {fieldB: valueB}]});
*/
db.users.find({ $or: [{firstName: "Neil"},{age:21}]});
// $$or with $gt in age
db.users.find({$or:[{firstName: "Neil"}, {age: {$gt: 21}}]});

// $and Operator
/*
	- allows us to find documents matching all criteria from multiple provided search criteria

	SYNTAX:
		db.collectionName({ $and: [ {fieldA: valueA}, {fieldB: valueB}]});
*/
db.users.find({ $and: [{age: { $ne: 82 }},{ age:{ $ne: 76}}]});

// SECTION
// Field Projection
/*
	- retrieving documnets are common operations that we do and by default MongoDB queries return the whole document as a response
	- when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish
	- to hhelp with readability of the values when  returned, we can include/exclude fields from the response
*/
// Inclusion
/*
	- allows us to include specific fields only when retrieving documents
	- value provided is 1 to denote that the field is being excluded
	
	// this code is applicable
	db.users.find(
	{ firstName: "Jane" },
	{
		firstName: true,
		lastName: true,
		contact: true
	}
);
*/
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)

// Exclusion
/*
	- allows us to exclude specific fields only when retrieving documents
	- value provided is 0 to denote that the field is being excluded

	// this code is applicable
	db.users.find(
	{ firstName: "Jane" },
	{
		firstName: false,
		lastName: false,
		contact: false
	}
*/

db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
);

// ID Suppression
db.users.find(
	{firstName: "Jane"},
	{
		_id: 0,
        firstName: 1,
        lastName: 1,
        contact: 1
	}
);
// Exclusion of Object 
db.users.find(
	{firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		"contact.person": 1
	}
);
// Suppressing Specific Fields in Embedded Documents
db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 0
	}
)
// Project Specific Array Elements in the Returned Array

// $slice Operatos
/*
	- returns a new array based on existing array
*/
db.users.find(
	
		{"nameArr": {nameA: "Juan"}},
		{
			nameArr: {$slice:1}
		}
);
// SECTION
// $regex Operator
/*
	- allows to find documents that match a specific string pattern using regular expressions

	SYNTAX:
		db.collectionName.find({field: {$regex: "pattern",$options: "$optionsValue"}});
*/
// Case-insensitive Query
db.users.find({firstName: {$regex: "N"}});
db.users.find({firstName: {$regex: "n", $options: "$i"}});
// "$i" is 'case insensitive'
db.users.find({$or:[{firstName: { $regex: "n"},{firstName: { $regex: "N"}]});
// same but using Or. can be used in